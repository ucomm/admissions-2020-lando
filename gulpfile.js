const gulp = require('gulp')
const webpack = require('webpack-stream')

const del = require('del')

const pluginOpts = {
  DEBUG: true,
  camelize: true,
  lazy: true
}

const plugins = require('gulp-load-plugins')(pluginOpts)

const sassPath = process.env.NODE_ENV === 'production' ?
  '/wp-content/themes/admissions-theme' :
  '/content/themes/admissions-theme'

const paths = {
  styles: {
    src: 'src/sass/**/*.scss',
    dest: process.env.NODE_ENV === 'production' ? 'build/css/' : 'dev-build/css/',
    includePaths: [
      './node_modules'
    ]
  },
  js: {
    src: 'src/js/**/*.js',
    dest: process.env.NODE_ENV === 'production' ? 'build/js/' : 'dev-build/js/'
  },
}
const onError = (err) => console.log(`Error -> ${err}`);

const clean = () => del(['build'])

/**
 * 
 * Get all styles from src/ and compress them
 * Ensures that CSS grid will be compatible with IE
 * 
 */
const styles = () => {
  return gulp.src(paths.styles.src)
    .pipe(plugins.plumber({ errorHandler: onError }))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.header(`$content-path: "${sassPath}";`))
    .pipe(plugins.sass({
      includePaths: paths.styles.includePaths,
      outputStyle: 'compressed'
    }))
    .pipe(plugins.autoprefixer({
      grid: "autoplace"
    }))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.styles.dest))
}

const scripts = () => {
  return gulp.src(paths.js.src)
    .pipe(plugins.plumber({ errorHandler: onError }))
    .pipe(plugins.sourcemaps.init())
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.js.dest))
}

gulp.task('clean', gulp.series(clean))

gulp.task('watch', () => {
  gulp.watch(
    [paths.styles.src, paths.js.src],
    { ignoreInitial: false },
    gulp.series(styles, scripts)
  )
})

gulp.task('default', gulp.parallel(styles, scripts))