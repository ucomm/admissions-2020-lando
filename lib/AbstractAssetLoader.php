<?php 

namespace Admissions2020\Lib;

abstract class AbstractAssetLoader {
  protected $handle;
  protected $adminHandle;
  protected $scriptsPath;
  protected $stylesPath;

  public function __construct() {
    $this->handle = $this->setHandle();
    $this->adminHandle = $this->setAdminHandle();

    if (strpos($_SERVER['HTTP_HOST'], 'admissions.uconn.edu') === false) {
      $this->scriptsPath = ADMISSIONS_2020_URL . '/dev-build/js/index.js';
      $this->stylesPath = ADMISSIONS_2020_URL . '/dev-build/css/main.css';
    } else {
      $this->scriptsPath = ADMISSIONS_2020_URL . '/build/js/index.js';
      $this->stylesPath = ADMISSIONS_2020_URL . '/build/css/main.css';
    }
  }

  /**
   * Enqueue the assets defined by the enqueue method.
   *
   * @return void
   */
  public function enqueueAssets() {
    add_action('wp_enqueue_scripts', [$this, 'enqueue']);
  }

  public function enqueueAdminAssets() {
    add_action('admin_enqueue_scripts', [$this, 'adminEnqueue']);
  }

  /**
   * Register and enqueue styles or scripts
   *
   * @return void
   */
  abstract function enqueue();

  abstract function adminEnqueue(string $hook);

  protected function setHandle() {
    return 'admissions-2020';
  }

  protected function setAdminHandle() {
    return 'admissions-2020-admin';
  }
}