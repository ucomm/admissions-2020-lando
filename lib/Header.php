<?php

namespace Admissions2020\Lib;

use Alert_Banner;
use UConn\Banner\Banner;
use Admissions2020\Lib\Menus;

class Header {

  private $menus;

  public function __construct() {
    $this->menus = new Menus();
  }

  public function createMainNavigation() {
    return $this->menus->createMainNavigationMenu();
  }

  public function createMobileNavMenu() {
    return $this->menus->createMobileNavMenu();
  }

  public function createTitleAreaMenu(string $id = 'title-area-menu') {
    return $this->menus->createTitleAreaMenu($id);
  }

  public function displayBanner() {
    $banner = new Banner();
    $banner->show_header = false;
    $banner->alternative = true;
    $banner->school = 'University of Connecticut';
    $banner->school_abbreviation = '';
    $banner->url = $this->getSchoolURL();
    $banner->a_z_dropdown = false;
    $banner->invert_banner_color = false;
    echo $banner;
  }

  public function createMetaTags() {
    ob_start();
    include(ADMISSIONS_2020_DIR . '/includes/header/meta-tags.php');
    echo ob_get_clean();
  }

  public function doCookieNotice() {
    ob_start();
    include(ADMISSIONS_2020_DIR . '/includes/header/cookie-notice.php');
    echo ob_get_clean();
  }

  public function doAlertBanner(): bool {
    $banner_exists = class_exists(Alert_Banner::class);
    $can_show_alerts = method_exists(Alert_Banner::class, 'show_alerts_in_content');

    if ($banner_exists && $can_show_alerts) {
      Alert_Banner::show_alerts_in_content();
      return true;
    } else {
      return false;
    }

  }

  private function getSchoolURL() {
    $url = '';

    if ($_SERVER['HTTP_HOST'] === 'localhost') {
      $url = 'http://localhost';
    } else {
      $url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    } 

    return $url;
  }
}