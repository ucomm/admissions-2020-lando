<?php

namespace Admissions2020\Lib\Admin;

class Options {
  public function createOptions() {
    add_action('admin_init', [ $this, 'prepareSettingSections' ]);
  }

  public function prepareSettingSections() {

  }

  public function registerSettings() {
    
  }

}