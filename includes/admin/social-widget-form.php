<?php
$select_field_id = $this->get_field_id('social-icon');
$select_field_name = $this->get_field_name('social-icon');
$link_field_id = $this->get_field_id('social-link');
$link_field_name = $this->get_field_name('social-link');
$a11y_title_id = $this->get_field_id('a11y-title');
$a11y_title_name = $this->get_field_name('a11y-title');
$icon = !empty($instance['social-icon']) ? $instance['social-icon'] : '';
$link = !empty($instance['social-link']) ? esc_url($instance['social-link']) : '';
$a11y_title = !empty($instance['a11y-title']) ? $instance['a11y-title'] : '';
?>

<div id="social-widget-container-<?php echo $this->id; ?>">
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $select_field_id; ?>">
        Social Icon
      </label>
    </div>
    <div class="field-wrapper">
      <select name="<?php echo $select_field_name; ?>" id="<?php echo $select_field_id; ?>" class="social-widget-select" data-icon-preview-id="social-icon-preview-<?php echo $this->id; ?>">
        <option value="">Select a social media type.</option>
        <option value="email-subscribe" <?php selected($icon, 'email-subscribe'); ?>>Email Subscribe</option>
        <option value="facebook" <?php selected($icon, 'facebook'); ?>>Facebook</option>
        <option value="flickr" <?php selected($icon, 'flickr'); ?>>Flickr</option>
        <option value="instagram" <?php selected($icon, 'instagram'); ?>>Instagram</option>
        <option value="linkedin" <?php selected($icon, 'linkedin'); ?>>LinkedIn</option>
        <option value="snapchat" <?php selected($icon, 'snapchat'); ?>>Snapchat</option>
        <option value="twitter" <?php selected($icon, 'twitter'); ?>>Twitter</option>
        <option value="youtube" <?php selected($icon, 'youtube'); ?>>YouTube</option>
      </select>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <span>Icon Preview</span>
    </div>
    <div class="field-wrapper">
      <div id="social-icon-preview-<?php echo $this->id; ?>" class="icon-preview-container">
      </div>
      <p><small>This area is for preview only. Styles will be different on the frontend.</small></p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $link_field_id; ?>">
        Social Icon Link
      </label>
    </div>
    <div class="field-wrapper">
      <input type="text" name="<?php echo $link_field_name; ?>" id="<?php echo $link_field_id; ?>" value="<?php echo $link; ?>" placeholder="https://example.com/uconn" />
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $a11y_title_id; ?>">
        Accessible Link Text
      </label>
    </div>
    <div class="field-wrapper">
      <input type="text" name="<?php echo $a11y_title_name; ?>" id="<?php echo $a11y_title_id; ?>" value="<?php echo $a11y_title; ?>" placeholder="Come visit UConn on social media" />
    </div>
  </div>
</div>