#!/bin/bash

# install composer dependencies for the theme itself.
composer install

# once that's done, find all installed plugins and themes and run these commands

# this command will
# - find in the plugins directory
# - all the immediate sub-directories (plugins...)
# - excluding the plugin directory itself
# - then execute a bash command to 
# - - go into each one
# - - if a composer.json file exists
# - - run composer install ignoring dev dependencies and scripts
find ./www/content/plugins -maxdepth 1 -type d ! -name . -exec bash -c "cd '{}' && ls composer.json && composer install --no-dev --no-scripts" \;