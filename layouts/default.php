<?php

use Admissions2020\Lib\ContentManager;

$content_manager = new ContentManager();
$content_manager->getHeader();

?>

<main>
  <section class="row">
    <?php 
      $content_manager->loop('template-parts/content', 'loop'); 
    ?>
  </section>
</main>

<?php $content_manager->getFooter(); ?>