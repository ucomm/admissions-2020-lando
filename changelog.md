# 1.0.3
- Update to a11y-menu nav script

# 1.0.2
- Fix to banner search for to direct searches to the correct site
- Fix to mobile menu for nested menus
- Updated styles for slate form integration

# 1.0.1
- Changed theme name and associated variables by request
- Updates to banner for improved site search

# 1.0.0
- Initial release