# Experimental WP Theme/Lando Project

**This project is not for production**

## Description
This project is an attempt to see if we can simplify our local WP development environment. Lando creates an abstraction layer over docker. This way instead of installing (and knowing): docker, composer, nvm/node/npm, etc... a developer will just need to install and run lando. Of course knowing about those other tools will still be helpful. But just to get started, it should remove a significant amount of onboarding time.


## Usage
- [Install lando](https://docs.lando.dev/basics/installation.html). Make sure to follow the instructions regarding whether or not to install Docker Desktop.
- run `lando start`
- work on the project

### What did that do...?
The `lando start` command will 
- look at the `.lando.yml` file where configuration is maintained
- get docker images as needed from docker hub based on the lando wordpress recipe
- start running those images
- set up services based on additional configuration to customize the recipe
  - install composer dependencies
  - surface wp cli and make adjustments to wp-config
  - create additional tooling that can be used with the `lando` command (e.g. `lando npm` or `lando composer`)
- when all services are ready, run the application in the background

